<?php
get_header();

?>
    <div class="container">
        <div class="row">
            <div id="content" class="col-8">
                <?php Sprig::render([],'cart'); ?>
            </div>
            <div id="sidebar" class="col-4">
                <?php get_sidebar();?>
            </div>
        </div>
    </div>
<?php
get_footer();