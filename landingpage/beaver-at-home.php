<?php
/*
Template Name: Beaver At Home Landing Page
*/
$yield =
	'<a href="http://beavergumballmachines.com/" class="beaver-at-home-button" target="_blank">Click here to visit the<br>BEAVER AT HOME<br>WEBSITE</a>';

$c = new Content_Loader();
$d = $c->get_data();

?>

<div class="container" id="main-content">
	<div id="content" class="col-9">
		<div class="row">
			<div class="col-12">
				<div class="page-title"><?php echo the_title(); ?></div>
				<h1><?php echo Sprig::custom_content_title( "Blue Heading Text" ); ?></h1>
				<?php echo Sprig::custom_content_block( "Secondary Subtitle Text" ); ?>
				<?php echo $yield; ?>
			</div>
		</div>
	</div>
	<div id="sidebar" class="col-3">
		<?php get_sidebar(); ?>
	</div>
</div>
