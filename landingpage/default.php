<?php
$bucketPods = pods( 'bucket', [
	'limit'   => 4
] );
while( $bucketPods->fetch() ) {
	$buckets[] = podProcessBucket( $bucketPods );
}

?>
<div class="language-selector container"><?php Sprig::render( [], 'language-selector' ); ?></div>
<div class="container">
	<div id="bucketContainer" class="container">
		<img height="113" width="180" style="" class="theresMoreBelow" alt="There's more below"
		     src="<?php echo ACTIVE_THEME_PATH_IMAGES ?>billboard/pages/theres-more-below.png">

		<div class="row">
			<?php for ( $i = 0; $i < 4; $i ++ ) : ?>
				<div id="buckets" class="col-xs-3">
					<?php
					Sprig::render( $buckets[ $i ], 'bucket' );
					?>
				</div>
			<?php endfor; ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php
			$content_with_arrow_data = [
				'left'  => [
					'class'   => 'left',
					'content' => Sprig::custom_content_block( "Content left of arrow" ),
				],
				'right' => [
					'class'   => 'right',
					'content' => Sprig::custom_content_block( "Content right of arrow" )
				]
			];

			Sprig::render( $content_with_arrow_data, 'content-with-arrow' );
			?>
		</div>
		<div class="row">
			<div id="content" class="col-12">
				<h1><?php echo Sprig::custom_content_title( "Blue subtitle text" ); ?></h1>
			</div>
		</div>
		<div class="row">
			<div id="content" class="col-6">
				<!--				--><?php //Sprig::render(); ?>
				<?php whatsNewAtBeaver(); ?>
				<h2><?php echo Sprig::custom_content_title( "Secondary content title" ) ?></h2>
				<?php
				$sites_list_data = get_all_sites();

				Sprig::render( $sites_list_data, 'sites-list' );
				?>
			</div>
			<div class="col-6 homepageSidebar">
				<?php Sprig::shortcode( '[contact-form-7 title="Newsletter Sign-up"]' ) ?>
			</div>
		</div>
	</div>
</div>
<?php
