<?php
/*
Template Name: Machine-O-Matic Landing Page
*/

// In content product slider

$slide        = pods( 'product_rotator', array() );
$total_slides = $slide->total();

if( $total_slides < 1 ) {
	$hide = true; // if no pods are returned, we will want to hide the billboard
}

while( $slide->fetch() ) {

	//var_dump($slide->display('background_color')); die();

	$array[] = [
		'image' => $slide->display( 'product_image' )
	];
}

// Page to only ever have 1 slide
$slides_data = [
	'local' => [
		'class'    => 'rotator-class',
		'allclass' => 'all-slides',
		'slides'   =>
			$array

	]
];

/*
 */

// Yield the final slider html to be feed into the wp template
$c = new Content_Loader();
$d = $c->get_data();

?>

<div class="container" id="main-content">
	<div id="content" class="col-9">
		<div class="row">
			<div class="col-12">
				<h1><?php echo Sprig::custom_content_title( "Blue subtitle text" ); ?></h1>

				<p><?php echo Sprig::custom_content_title( "Top Tagline" ); ?></p>

				<div class="bullet-list col-7">
					<?php
					for( $i = 1; $i < 4; $i++ ) {
						echo '<h2 class="content-h2">' . Sprig::custom_content_title( 'Title ' . $i ) . '</h2>';
						echo '<div class="list">' . Sprig::custom_content_block( 'List ' . $i ) . '</div>';
					}
					?>
				</div>

				<div class="col-5">
					<div class="machineOMaticSlider">
						<?php Sprig::render( $slides_data, 'responsiveslides-body' ); ?>
					</div>
				</div>
			</div>
			<div class="col-12">
				<h3 class="large-h3"><?php echo Sprig::custom_content_title( "Blue heading bottom" ); ?></h3>
			</div>
		</div>
	</div>
	<div id="sidebar" class="col-3">

		<?php get_sidebar(); ?>
	</div>
</div>
