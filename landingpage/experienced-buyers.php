<?php
$bucketPods = pods( 'bucket', [] );
while( $bucketPods->fetch() ) {
	$buckets[] = podProcessBucket( $bucketPods, true );
}

$sidebars    = [ ];
$sidebarPods = pods( 'sidebar_item', [
	'orderby' => 'list_order'
] );
while( $sidebarPods->fetch() ) {
	$sidebars[] = podProcessSidebar( $sidebarPods, true );
}
?>

<div class="container">
	<div class="row">
		<div id="content" class="col-12">
			<h1><?php echo Sprig::custom_content_title( "Blue subtitle text" ); ?></h1>
		</div>
	</div>
	<div class="row">
		<div id="content" class="col-6">
			<?php modalCorporateVideo() ?>
			<?php whatsNewAtBeaver(); ?>
		</div>
		<div class="col-6 homepageSidebar">

			<?php Sprig::shortcode( '[contact-form-7 title="Newsletter Sign-up"]' ) ?>
		</div>
	</div>
	<div class="row footerBars">
<!--		--><?php //if ( $sidebars[0] ) {
//			Sprig::render( $sidebars[0], 'sidebar' );
//		} ?>
		<?php if ( $sidebars[0] ) {
            $sidebars[1]['content']     = 'Looking for help? Want to ask a question?<br><br>
				Available Monday to Thursday<br> 8:30 am to 4:00 PM EST,<br>
				located at the bottom of your screen.';
            $sidebars[1]['buttonLabel'] = '';
			Sprig::render( $sidebars[1], 'sidebar' );
		} ?>
		<?php if ( $sidebars[0] ) {
			Sprig::render( $sidebars[2], 'sidebar' );
		} ?>
	</div>
</div>
