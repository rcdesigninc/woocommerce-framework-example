<?php
$bucketPods = pods( 'bucket', [

] );
while( $bucketPods->fetch() ) {
	$buckets[] = podProcessBucket( $bucketPods, true );
}

$sidebars    = [];
$sidebarPods = pods( 'sidebar_item', [
	'orderby' => 'list_order'
] );
while( $sidebarPods->fetch() ) {
	$sidebars[] = podProcessSidebar( $sidebarPods, true );
}
?>

<div class="container">

	<div class="row">
		<div id="content" class="col-12">
			<h1><?php echo Sprig::custom_content_title( "Blue subtitle text" ); ?></h1>
		</div>
	</div>

	<div class="scrollSelect">
		<div id="bucket-carousel" class="carousel slide" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<div class="row">
						<?php for( $i = 0; $i < 3; $i++ ) : ?>
							<div id="buckets" class="col-xs-4">
								<?php
								Sprig::render( $buckets[$i], 'bucket' );
								?>
							</div>
						<?php endfor; ?>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<?php for( $i = 3; $i < 6; $i++ ) : ?>
							<div id="buckets" class="col-xs-4">
								<?php
								Sprig::render( $buckets[$i], 'bucket' );
								?>
							</div>
						<?php endfor; ?>
					</div>
				</div>
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#bucket-carousel" role="button" data-slide="prev">
				<span><img src="<?php echo ACTIVE_THEME_PATH_IMAGES ?>buckets/prev-arrow.png"></span>
			</a>
			<a class="right carousel-control" href="#bucket-carousel" role="button" data-slide="next">
				<span><img src="<?php echo ACTIVE_THEME_PATH_IMAGES ?>buckets/next-arrow.png"></span>
			</a>
		</div>

	</div>

	<div class="row">
		<div id="content" class="col-6">
			<?php whatsNewAtBeaver(); ?>
			<h2><?php echo Sprig::custom_content_title( "Secondary content title" ) ?></h2>
			<?php
			//$sites_list_data = get_all_sites();

			//Sprig::render( $sites_list_data, 'sites-list' );

            $products = Sprig::get_posts('product');

            if(count($products)) : ?>
            <section role="sites-list">
                <?php foreach($products as $product) : ?>
                    <?php
                    $product_description = trim(preg_replace("/<[\\/]*p>/","",$product->post_content));
                    if(!preg_replace("/[^a-z0-9]/i","",$product_description)){
                        continue;
                    }
                    ?>
                    <article class="site site-{{site.id}} site-{{site.slug}}">

                        <p><a href="<?php echo $product->guid;?>"><?php echo $product->post_title;?></a> <?php echo $product_description;?></p>
                    </article>
                <?php endforeach; ?>
            </section>

            <?php endif; ?>
		</div>
		<div class="col-6 homepageSidebar">
			<?php modalCorporateVideo() ?>
			<?php Sprig::shortcode( '[contact-form-7 title="Newsletter Sign-up"]' ) ?>
		</div>
	</div>
	<div class="row footerBars">
		<?php if( $sidebars[0] ) {
			Sprig::render( $sidebars[0], 'sidebar' );
		} ?>
		<?php if( $sidebars[0] ) {
            $sidebars[1]['content']     = 'Looking for help? Want to ask a question?<br><br>
				Available Monday to Thursday<br> 8:30 am to 4:00 PM EST,<br>
				located at the bottom of your screen.';
			$sidebars[1]['buttonLabel'] = '';
			Sprig::render( $sidebars[1], 'sidebar' );
		} ?>
		<?php if( $sidebars[0] ) {
			Sprig::render( $sidebars[2], 'sidebar' );
		} ?>
	</div>
</div>
