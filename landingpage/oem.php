<?php
/*
Template Name: OEM Landing Page
*/
get_header();

$c = new Content_Loader();
$d = $c->get_data();

?>
<div id="main-content" class="container">
	<div id="content" class="col-9">
		<div class="row">
			<div class="col-12">
				<h1><?php echo Sprig::custom_content_title( "Blue subtitle text" ); ?></h1>
				<?php echo Sprig::custom_content_block( "Secondary subtitle text" ); ?>
				<?php echo Sprig::custom_content_title( "Secondary subtitle red text" ); ?>
			</div>
		</div>
	</div>
	<div id="sidebar" class="col-3">
		<?php get_sidebar(); ?>
	</div>
</div>
