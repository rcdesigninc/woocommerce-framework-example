<?php
get_header();


//Get page content for this "page" before showing posts
$mainHeading = Sprig::custom_content_title( "Main Heading" );
$mainContent = Sprig::custom_content_block( "Main Content" );



?>

<div class="container" id="main-content">
	<div class="row">
		<div id="content" class="col-8">
			<div class="page-title">Blog</div>
			<?php
			if( $mainHeading ) {
				?>
				<h1><?php echo $mainHeading; ?></h1>
			<?php
			}
			echo $mainContent;

			?>

			<?php Sprig::render($data,'blog-posts'); ?>
        </div>
        <div id="sidebar" class="col-md-4">
            <?php get_sidebar();?>
        </div>
    </div>
</div>
<?php
get_footer();