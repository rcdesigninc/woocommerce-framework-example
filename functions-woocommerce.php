<?php

if(!function_exists('reset_woocommerce_single_product_summary')) {
    function reset_woocommerce_single_product_summary()
    {
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
    }
}

if(!function_exists('jk_dequeue_styles')) {
    function jk_dequeue_styles($enqueue_styles)
    {
        unset($enqueue_styles['woocommerce-general']);    // Remove the gloss
        return $enqueue_styles;
    }

    add_filter('woocommerce_enqueue_styles', 'jk_dequeue_styles');
}

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

if(!function_exists('woocommerce_template_product_description')) {
    function woocommerce_template_product_description()
    {
        woocommerce_get_template('single-product/tabs/description.php');
    }

    add_action('woocommerce_single_product_summary', 'woocommerce_template_product_description', 20);

    add_filter('woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);
    add_filter('woocommerce_product_tabs', 'sb_woo_remove_description_tab', 98);
}

if(!function_exists('sb_woo_remove_reviews_tab')) {
    function sb_woo_remove_reviews_tab($tabs)
    {

        unset($tabs['reviews']);

        return $tabs;
    }
}

if(!function_exists('sb_woo_remove_description_tab')) {
    function sb_woo_remove_description_tab($tabs)
    {

        unset($tabs['description']);

        return $tabs;
    }
}

get_template_part('classes/wc-product-customizer');

add_filter( 'product_type_selector' , 'add_product_machine_product_type' );

function add_product_machine_product_type( $types ){
    $types[ 'product_machine' ] = __( 'Beaver machine' );
    return $types;
}

function add_to_cart_custom_data( $cart_item_key, $product_id = null, $quantity= null, $variation_id= null, $variation= null ) {
    $custom_data = Woocommerce_Product_Customizer::get_product_custom_data($cart_item_key);

    if(!empty($custom_data) && $custom_data->data){
        $data = $custom_data->data;

        foreach($data as $p => $option){
            foreach($option as $o => $value){
                $session_key = $cart_item_key.'_'.$p.'.'.$o;
                $session_value = get_post($value)->post_title;
                WC()->session->set( $session_key, $session_value);
            }
            WC()->session->set( $cart_item_key."_custom_data", "set");
        }
    }
}

function add_order_item_custom_data( $item_id, $values, $cart_item_key ) {
    $custom_data = Woocommerce_Product_Customizer::get_product_custom_data($cart_item_key);

        if(!empty($custom_data) && $custom_data->data){
            $data = $custom_data->data;

            foreach($data as $p => $option){
                $additional_costs = [];

                foreach($option as $o => $value){
                    $clean_label = Woocommerce_Product_Customizer::clean_label($o);
                    $label = $clean_label; //$p." ".$clean_label;

                    $value_post = get_post($value);
                    $value_post->post_meta = get_post_meta($value);

                    $value_price = (float)$value_post->post_meta['price'][0];

                    if($value_price){
                        $additional_costs[] = [
                            'label' => ucwords($clean_label),
                            'price' => number_format($value_price, 2)
                        ];
                    }

                    wc_add_order_item_meta( $item_id, $label, $value_post->post_title );
                }

                $additional_cost_value = [];

                foreach($additional_costs as $i => $v){
                    $additional_cost_value[] = $v['label'].': +CAD$'.$v['price'];
                }

                if(count($additional_cost_value)){
                    $additional_cost_value = implode(', ',$additional_cost_value);

                    wc_add_order_item_meta( $item_id, get_option('order_settings_additional_costs_label'), $additional_cost_value);
                }
            }
        }
}

add_action( 'woocommerce_add_order_item_meta', 'add_order_item_custom_data', 1, 3 );
