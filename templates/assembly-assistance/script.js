jQuery( document ).ready( function( $ ) {
	var $fileListings = $(".file-listing");
	$(".assembly-link").each(function () {
		var $this = $(this);
		var $assemBox = $this.closest('.stand-box');
		var hrefPiece = '[id=' + $this.attr('data-entry') + ']';

		console.log(hrefPiece);

		var $closeButton = $('<div class="close-btn">CLOSE</div>');
		$closeButton.click(function () {
			var $active = $fileListings.filter(hrefPiece);
			$active.slideUp('fast');
		});

		$this.parent().parent().next().append($closeButton);

		$this.click(function (e) {
			var $active = $fileListings.filter(hrefPiece);
			$fileListings.not($active).slideUp('fast');

			$fileListings.filter(hrefPiece).slideDown();

			setTimeout(function () {
				scrollFrom($assemBox);
			}, 200);

			e.preventDefault();
			return false;
		});
	});

	$('.modal-panel .close' ).on('click',function(e) {
		$(this ).closest('.modal-dialog' ).find('iframe' ).attr('src', $(this ).closest('.modal-dialog' ).find('iframe' ).attr('src'));
	});
} );
