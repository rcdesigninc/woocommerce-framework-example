<?php
/** link additional template scripts */
if( !function_exists( 'responsiveslides_scripts' ) ) {
	function responsiveslides_scripts() {
		return [
			'js/responsiveslides.min.js'
		];
	}
}
