<?php
get_header();
?>
	<div class="container" id="main-content">
		<div class="row">
			<div id="content" class="col-8">
				<div class="pageTitleHeadingContent">
					<div class="page-title"><?php the_title(); ?></div>
					<?php
					$mainHeading = Sprig::custom_content_title( "Main Heading" );
					$mainContent = Sprig::custom_content_block( "Main Content" );

					if( $mainHeading ) {
						?>
						<h1><?php echo $mainHeading; ?></h1>
					<?php
					}
					echo $mainContent;

					render_associated_pod( $post->post_name );
					?>
				</div>
				<?php //Sprig::render(); ?>
			</div>
			<div id="sidebar" class="col-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
get_footer();
