jQuery(function($){
    var ajax_dir = "/wp-content/themes/beavervending/ajax/";

    $(document).ready(function(){
        $(".entry-summary").find("[role='custom-product-data']").change(function(e){
            var t = $(this);
            var name = t.attr('name');

            switch(name){
                case 'region':
                case 'contents':
                    $.ajax({
                        url:ajax_dir+'woocommerce.php',
                        type:'post',
                        dataType:'json',
                        data:{
                            action: name,
                            value: t.val()
                        },
                        complete:function(r){
                            var data = $.parseJSON(r.responseText);
                            update_target(name,data);
                        }
                    });
                    break;
                default:
                    break;
            }
        });
    });
    function update_target(obj,data){
        var target = (obj == 'region') ? 'mechs' : 'dispenser';

        target = $(target);

        for(var d in data){
            target.append("<option value='"+ d.value +"'>"+ d.label +"</option>")
        }
    }
},jQuery);

