<?php
$price_adjust_class = __DIR__.'/product-price-adjustment.php';
if(file_exists($price_adjust_class)){
    include($price_adjust_class);
}

class Woocommerce_Product_Customizer {
    static $instance;
	protected $db_version = 0.8;
	protected $db_table = 'wc_product_customizer';

    public static function register() {
        if (self::$instance == null) {
            self::$instance = new Woocommerce_Product_Customizer();
        }
    }

    public function __construct() {
        add_filter('woocommerce_get_cart_item_from_session', [$this, 'get_cart_item_from_session'], 10, 3);
        add_action('woocommerce_add_to_cart',[$this,'set_custom_product_data'],10,3);
        add_filter('woocommerce_add_cart_item_data', array($this, 'wc_add_cart_item_data'), 10, 2);

	    $this->check_db_table();
    }


    function set_custom_product_data($cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data){
        if(isset($_POST['custom_data']) && $data = $_POST['custom_data']){
            $price_adjustment = 0;

            if(class_exists("Product_Price_Adjustment")){
                $price_adjustment = Product_Price_Adjustment::get_price_adjustment($data);
            }

            $this->insert($cart_item_key,$data,$price_adjustment);
        }
    }

	static function get_product_custom_data($cart_item_id){
		$s = new self();

		$return = $s->get($cart_item_id);

        $return->data = unserialize($return->data);

        return $return;
	}

    function wc_add_cart_item_data($cart_item_meta, $product_id) {
        global $woocommerce;

        $cart_item_meta['_unique_id'] = uniqid();

        return $cart_item_meta;
    }

	static function insert_price_adjust($cart_item_id,$price_adjust,$data = array()) {
		$s = new self();
		$s->insert($cart_item_id,$data,$price_adjust);
	}

	function add_cart_item_data($cart_item_meta, $product_id){
		global $woocommerce;

		$product = new WC_Product( $product_id);
		$price = $product->price;

		if(empty($cart_item_meta['_other_options']))
			$cart_item_meta['_other_options'] = array();

		$cart_item_meta['_other_options']['product-price'] = $price;

		return $cart_item_meta;
	}

	function get_cart_item_from_session($cart_item, $values, $cart_item_id){
        if(!isset($GLOBALS[$cart_item_id.'_adjusted'])) {
            $price_adjust = $this->get_price_adjust($cart_item_id);

            $cart_item['data']->adjust_price($price_adjust);

//        $cart_item['testfeld'] = $values['test_field'];

            if (!WC()->session->get($cart_item_id . '_custom_data')) {
                add_to_cart_custom_data($cart_item_id);
            }

            /*        if(!isset($_POST['wall_type'])){
                        echo "******";
                        echo WC()->session->get( $cart_item_id.'_length');
                        echo WC()->session->get( $cart_item_id.'_width');
                        echo WC()->session->get( $cart_item_id.'_depth');
                        echo WC()->session->get( $cart_item_id.'_wall_type');
                        echo "******";
                    }*/

            $GLOBALS[$cart_item_id.'_adjusted'] = true;
        }
		return $cart_item;
	}

	function get_price_adjust($cart_item_id){
		$result = $this->get($cart_item_id)->price_adjust;

		return $result;
	}

    public static function clean_label($string){
        return preg_replace("/[^a-z0-9\\.]+/i"," ",$string);
    }

    public static function clean_custom_data_value($string){
        if(is_numeric($string)){
            return ((float)$string)." inches";
        }
        else{
            return self::clean_label($string);
        }
    }

    function set_order_id($order_id){

    }

	/*** install/setup/db access ***/
	function check_db_table() {
		if ( get_site_option( 'wc_product_customizer_db_version' ) != $this->db_version ) {
			$this->create_db_table();
		}
	}

	function create_db_table() {
		global $wpdb;

		$table_name = $wpdb->prefix . $this->db_table;

		$charset_collate = '';

		if ( ! empty( $wpdb->charset ) ) {
			$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		}

		if ( ! empty( $wpdb->collate ) ) {
			$charset_collate .= " COLLATE {$wpdb->collate}";
		}

		$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		`cart_item_id` varchar(32) DEFAULT '' NOT NULL,
		`data` MEDIUMTEXT,
		`price_adjust` DECIMAL (20,4) DEFAULT 0 NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

		add_option( $this->db_table.'_db_version', $this->db_version );
	}

	function insert($cart_item_id,$data,$price_adjust){
		global $wpdb;

		$table_name = $wpdb->prefix . $this->db_table;

		$wpdb->insert(
			$table_name,
			[
				'cart_item_id' => $cart_item_id,
				'data' => serialize($data),
				'price_adjust' => $price_adjust
			]
		);
	}

	function get($cart_item_id){
		global $wpdb;
		$table_name = $wpdb->prefix . $this->db_table;

        $result = $wpdb->get_results( "SELECT * FROM $table_name WHERE cart_item_id = '$cart_item_id' ORDER BY id DESC LIMIT 1", OBJECT );

        return $result[0];
	}
}

Woocommerce_Product_Customizer::register();

