<?php
class Product_Price_Adjustment{
	function __construct(){

	}

    public static function get_price_adjustment($data = []){
        $price_adjust = 0;

        if(!empty($data)){
            foreach($data as $d){
                foreach($d as $post_type => $post_id){
                    $post = get_post_meta($post_id);
                    if(isset($post['price'][0])){
                        $price_adjust += $post['price'][0];
                    }
                }
            }
        }

        return $price_adjust;
    }
}